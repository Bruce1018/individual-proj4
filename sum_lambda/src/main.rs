use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct CustomEvent {
    sequence: Vec<i32>,
}

#[derive(Serialize)]
struct CustomOutput {
    sum: i32,
}

async fn sum_handler(event: CustomEvent, _: Context) -> Result<CustomOutput, Error> {
    let sum = event.sequence.iter().sum();
    Ok(CustomOutput { sum })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    simple_logger::init_with_level(log::Level::Info)?;
    let func = handler_fn(sum_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
