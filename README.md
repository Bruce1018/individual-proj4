# individual project 4

> zx112  
> Bruce Xu

# General description

In this project, the first step(called `fibonacci_lambda`) is to generate the `fib sequence` of input integer. Second(called `sum_lambda`) returns the sum of these sequence.

# Demo
https://youtu.be/iPWEpcEoV1Y

# Steps 1: Write code

0. initialize the `git` and push on local 
1. edit `toml` file, add corresponding dependecies 
2. edit `main.rs` file

After this step, you should get two directories with corresponding `lambda functions` implementation. 

![](/截屏2024-04-22%2023.47.08.png)

# Step 2: Deploy on AWS

0. in each directory, run following commands
   1. `cargo lambda build --release`
   2. `cargo lambda deploy -- .....`

You can use the `iam` from previous projects

After this step, you should see following on your `aws lambda`

![](/截屏2024-04-23%2000.01.43.png)

# Step 3: Create Step functions

search `step functions` on aws, drag the lambda functions you just deploy

![](/截屏2024-04-23%2000.24.58.png)

then click `start execution` with input json, check the output in each step

![](/截屏2024-04-23%2000.26.11.png)

![](/截屏2024-04-23%2000.26.20.png)

