use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct CustomEvent {
    number: i32,
}

#[derive(Serialize)]
struct CustomOutput {
    sequence: Vec<i32>,
}

async fn fibonacci_handler(event: CustomEvent, _: Context) -> Result<CustomOutput, Error> {
    let result = fibonacci_sequence(event.number);
    Ok(CustomOutput { sequence: result })
}

fn fibonacci_sequence(n: i32) -> Vec<i32> {
    let mut sequence = Vec::new();
    let (mut a, mut b) = (0, 1);
    while a <= n {
        sequence.push(a);
        let temp = a;
        a = b;
        b = temp + b;
    }
    sequence
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    simple_logger::init_with_level(log::Level::Info)?;
    let func = handler_fn(fibonacci_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
